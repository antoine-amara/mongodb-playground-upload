DC = docker-compose

.SILENT: all up build vendor ps run rm destroy
default: help;

all:		## install the application from scratch, build containers, install dependencies and run them.
	${MAKE} build
	${MAKE} vendor
	${MAKE} up

.PHONY: all

up:		## up the application by running all containers.
	${DC} up -d
	${MAKE} ps

.PHONY: up

build:		## build the docker images, run this command when you change Docker image configuration in Dockerfile.
	${DC} build

.PHONY: build

vendor:		## install application dependencies for each containers.
	${DC} run --rm node npm install
	${DC} run --rm flask pip install -r ./requirements.txt

.PHONY: vendor

ps:		## display the status of all containers.
	${DC} ps

.PHONY: ps

logs:		## see applicative logs for one or more containers.
	${DC} logs ${args}

.PHONY: logs

run: 		## run a command inside a container
	${DC} run ${container} ${command}

.PHONY: run

restart:	## restart one or more container.
	${DC} restart ${container}

.PHONY: restart

rm:		## stop and remove the container but DON'T destroy the persistent datas.
	${DC} rm -f -v -s

.PHONY: rm

destroy:	## stop and destroy all containers with their PERSISTENT DATAS, PLEASE USE THIS CAUTION !!!
	${DC} down

.PHONY: destroy

help:		## Show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: help
