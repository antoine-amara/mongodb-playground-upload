# MongoDB upload playground

A playground environment powerd by docker to test uploads feature, like mongodb gridfs, easily. The environment is `production like` with https and traefik reverse proxy.

To see all available commands to run and manage this environment just keep calm and use `make`  command (or `make help` ).



## Contributing

You can push your experiments into this repo. Please create a branch from master just as following:

```sh
git checkout -b <yourname>-<yourlangage>-<experimentname>
# example
git checkout -b antoine-nodejs-tuto1gridfs
```

## Install and running

Just clone the repository if you doesn't and execute `make all` command.

The make all command will build docker images, install dependencies and up the containers, this environment contain 4 containers:

* traefik (reverse proxy)

* db (mongodb database)

* node (nodejs api)

* flask (python flask api)

## How to use the service

Traefik is the entrypoint for each webservice, and he define hostsname you can use to directly access nodejs or flask api and the mongodb instance if needeed. Next, I will describe in more details each container but now let's see the adress for each container.

* **traefik dashboard:** [traefik.playground.localhost](https://traefik.playground.localhost) 

* **nodejs api:** [nodejs.playground.localhost](https;//nodejs.playground.localhost) 

* **flask api:** [flask.playground.localhost](https;//flask.playground.localhost) 

* **mongodb:** [database.playground.localhost](https://database.playground.localhost) 

**WARNING:** If you are using a version of ubuntu <= 16.04 or MacOS, you have to add these lines into your `/etc/hosts` 

```sh
127.0.0.1			playground.localhost
127.0.0.1			monitor.playground.localhost
127.0.0.1			nodejs.playground.localhost
127.0.0.1			flask.playground.localhost
127.0.0.1			mongodb.playground.localhost
```

## Containers

In this section you will have all usefull informations about the containers.



### Traefik (container name: traefik)

traefik is the reverse proxy and load balancer, it just run a configuration file which is into `/Docker/traefik/config/config.toml` 



You can note the ssl certificate which use by traefik for https is in `/Docker/traefik/certificate` 



You can access to the dashboard here: [traefik.playground.localhost](https://traefik.playground.localhost) 



### MongoDB (container name: db)

The mongodb instance you can access to it into your code with the db name.



Note this service has an authentication:

```
database name: playground
database user: dreamquark
database password: dq22random
```



If you want to change these informations just update these environment variables into `docker-compose.yml`

```sh
MONGODB_APPLICATION_DATABASE=playground
MONGODB_APPLICATION_USER=dreamquark
MONGODB_APPLICATION_PASS=dq22random
```

If you update an environment variable, please don't forget to do a `make rm build up` to stop, remove, rebuilt the image and rerun your environment.



### Nodejs (container name: node)

The source code (index.js) and the package.json are into /src/node/

Note the nodejs container is running in inspect mode, you can rattach a chrome developper console with it in port 9042.

The nodejs process is running in development mode with a livereload.

Finally the entrypoint of the server is index.js



You can access to the API at: [nodejs.playground.localhost](https;//nodejs.playground.localhost) 



### Flask (container name: flask)

The source code (server.py) is available in /src/flask

The API is running in development mode with a livereload.

By default python 2.7 is running.



You can change the version into the docker-compose file:

```sh
PYTHON_VERSION: python:2.7-jessie -> PYTHON_VERSION: python:3.X-jessie
```

If you update an environment variable, please don't forget to do a `make rm build up` to stop, remove, rebuilt the image and rerun your environment.



You can access to the API at: [flask.playground.localhost](https;//flask.playground.localhost)


