from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "It Works !!!"

@app.route("/upload")
def upload():
    return "Upload route"

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
