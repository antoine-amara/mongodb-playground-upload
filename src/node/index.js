const http = require('http');

const PORT = 9000;

const server = http.createServer((req, res) => {
  const { url } = req;

  switch (url) {
    case '/':
      _interceptRoot(req, res);
      break;
    case '/upload':
      _interceptUpload(req, res);
      break;
    default:
      _interceptRoot(req, res);
  }
});

function _interceptRoot(req, res) {
  console.info('🐰 [NODEJS] intercepting root');
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify({ message: 'It Works !!!' }));
}

function _interceptUpload(req, res) {
  console.info('🐰 [NODEJS] intercepting upload');
  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify({ message: 'Upload route' }));
}

server.listen(PORT, _ => console.info(`🐰 [NODEJS] server listening on port ${PORT}`));
